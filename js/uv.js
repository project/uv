Drupal.uv = Drupal.uv || {};

(function ($) {
  Drupal.behaviors.uv = {
    attach: function () {
      var fields = Drupal.settings.uv.fields_to_validate;
      $.each(fields, function(fieldName, field) {
        var $fieldElement = $('#edit-'+field.js_name);
        var $input = $('input', $fieldElement);
        // Check if listener already exists.
        if($.data($input[0], 'events').keyup.some(function(listener) { return listener.namespace === 'uv';})) {
          // Continue.
          return true;
        }
        var fieldData = $fieldElement.data();
        $('input', $fieldElement).bind('input.uv', debounce(function() {
          var context = {
            entity_type: Drupal.settings.uv.entity_type,
            bundle: Drupal.settings.uv.bundle,
            node: { nid: Drupal.settings.uv.context.node.nid }
          };
          var $input = $(this);
          var state = {};
          if(fieldData.uv) {
            // Call pre-validation function.
            state = window['Drupal']['uv'][fieldData.uv](field.js_name, $input.val(), context) || {};
            if(state.type === 'error' || state.type === 'empty') {
              changeInputState($input, state)
            }
          }

          if(state.type !== 'error' && state.type !== 'empty') {
            // Pre-validation passed. Validate on server.
            changeInputState($input, {
              type: 'validating',
              info: 'validating_input',
              message: Drupal.t('Validating...')
            });
            debounce(function() {
              var success = false;
              var errorTimeout = setTimeout(function() {
                if (!success) {
                  changeInputState($input, {
                    type: 'error',
                    info: 'no-result',
                    message: Drupal.t('Validation failed because of server or network issues.')
                  });
                }
              }, 5000);
              $.getJSON(
                '/uv/validate/'+
                context.entity_type+'/'+
                context.bundle+'/'+
                context.node.nid+'/'+
                field.js_name+'/'+
                $input.val(),
                {},
                function (result) {
                  success = true;
                  clearTimeout(errorTimeout);
                  if (!result) {
                    console.error(new Error('Drupal Universal Validation module - No field validation result from server.'));
                    return;
                  }
                  if(result.state === 'failure') {
                    console.error(new Error('Drupal Universal Validation module - '+result.message));
                    return;
                  }

                  changeInputState($input, result);
                }
              );
            }, field.debounce_time, false)();
          }
        }, 200, false));
      });
      function debounce(func, wait, immediate) {
        var timeout;
        return function() {
          var context = this, args = arguments;
          var later = function() {
            timeout = null;
            if (!immediate) func.apply(context, args);
          };
          var callNow = immediate && !timeout;
          clearTimeout(timeout);
          timeout = setTimeout(later, wait);
          if (callNow) func.apply(context, args);
        };
      };
      function changeInputState($input, state) {
        $input.removeClass('error state warning validating empty');
        $input.addClass(state.type);
        var message_element = '<div class="uv-message messages '+state.type+' '+state.info+'">'+state.message+'</div>';
        var $messages = $input.siblings('.uv-message');
        if (state.type === 'empty' && !state.message) {
          $messages.remove();
        }
        if ($messages.length) {
          $messages.replaceWith(message_element);
        }
        else {
          var $fieldSuffix = $input.siblings('.field-suffix');
          var $insertPoint = $fieldSuffix.length ? $fieldSuffix : $input;
          $insertPoint.after(message_element);
        }
      }
    }
  };
})(jQuery);