/**
 * Pre-validation for identity number.
 *
 * Returned object form:
 * {
 *   'type' => string [allowed values: empty, error],
 *   'info' => string [added as CSS class to the message box],
 *   'message' => string [message in the message box]
 * }
 */
Drupal.uv.identity_number = function(fieldName, value) {
  // It's a good idea to define what happens if the value is emptied.
  // Setting the message to '' will hide the message.
  if (value !== '0' && !value) {
    return {
      type: 'empty',
      info: 'no-value',
      message: ''
    };
  }
  if(value.length !== 3 || !/^\d+$/.test(value)) {
    return {
      type: 'error',
      info: 'invalid-identity-number',
      message: Drupal.t('Identity numbers are three digits long.')
    };
  }
  // You don't need a return value when pre-validation passes.
  // Next UV waits for the server side validation.
}