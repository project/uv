<?php
/**
 * Universal Validation plugin.
 *
 * Returned array form:
 * array(
 *   'type' => string [allowed values: empty, warning, error, status],
 *   'info' => string [added as CSS class to the message box],
 *   'message' => string [message in the message box]
 * );
 */

$plugin = array(
  'label' => t('Identity number'),
  'handler' => array(
    'function' => 'uv_validate_identity_number',
    'js' => 'identity_number'
  ),
  'field_names' => ['field_identity_number'],
);

function uv_validate_identity_number($value, $settings) {
  // Remember to do pre-validation on server also.
  if ($value !== '0' && empty($value)) {
    return array(
      'type' => 'empty',
      'info' => 'no-value',
      'message' => '',
    );
  }
  if (strlen($value) !== 3 || !ctype_digit($value)) {
    return array(
      'type' => 'error',
      'info' => 'invalid-identity-number',
      'message' => t('Identity numbers are three digits long.'),
    );
  }
  // Here you should make a database query or something not possible in the
  // browser.
  if ($value === '123') {
    return array(
      'type' => 'error',
      'info' => 'taken-identity-number',
      'message' => t('This number is already taken.'),
    );
  }
  if ($value === '007') {
    return array(
      'type' => 'warning',
      'info' => 'reserved-identity-number',
      'message' => t('This number is reserved for mister Bond. Don\'t touch!'),
    );
  }
  return array(
    'type' => 'status',
    'info' => 'pass',
    'message' => t('Ok!'),
  );
}